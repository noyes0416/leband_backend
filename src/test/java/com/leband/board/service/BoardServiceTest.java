package com.leband.board.service;

import com.leband.board.dto.request.CreateBoardRequestDTO;
import com.leband.board.entity.NamingEntity;
import com.leband.board.entity.TBoardEntity;
import com.leband.board.repository.BoardRepository;
import com.leband.board.repository.NamingRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.refEq;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BoardServiceTest {

    @InjectMocks
    private BoardService boardService;

    @Mock
    private BoardRepository boardRepository;

    @Mock
    private NamingRepository namingRepository;

    @Test
    public void create_board_success() {
        //given
        CreateBoardRequestDTO requestDTO = CreateBoardRequestDTO.builder()
                .userNo(1L)
                .contactUrl("https://naver.com")
                .boardTitle("회색붕어빵")
                .build();

        //when
        boardService.createBoard(requestDTO);

        //then
        verify(boardRepository, times(1)).save(refEq(TBoardEntity.createInsertEntity(requestDTO)));
    }

    @Test
    public void testGetBandName() {
        List<NamingEntity> order1Names = new ArrayList<>();
        order1Names.add(createBandNameEntity("Band1-1"));
        order1Names.add(createBandNameEntity("Band1-2"));

        List<NamingEntity> order2Names = new ArrayList<>();
        order2Names.add(createBandNameEntity("Band2-1"));
        order2Names.add(createBandNameEntity("Band2-2"));

        when(namingRepository.findBySort(1)).thenReturn(order1Names);
        when(namingRepository.findBySort(2)).thenReturn(order2Names);

        String result = boardService.getBandName();

        // 반환된 결과가 기대하는 형태인지 확인
        String[] nameParts = result.split(" ");
        assertEquals(2, nameParts.length);

        // 반환된 결과가 Mock 데이터에 포함된 값인지 확인
        List<String> allNames = new ArrayList<>();
        allNames.add("Band1-1");
        allNames.add("Band1-2");
        allNames.add("Band2-1");
        allNames.add("Band2-2");

        for (String namePart : nameParts) {
            assert (allNames.contains(namePart));
        }

        // bandNameRepository.findBySortOrder() 메서드가 각각 1번씩 호출되었는지 확인
        verify(namingRepository, times(1)).findBySort(1);
        verify(namingRepository, times(1)).findBySort(2);
    }

    private NamingEntity createBandNameEntity(String name) {
        NamingEntity namingEntity = new NamingEntity();
        namingEntity.setName(name);
        return namingEntity;
    }
}
