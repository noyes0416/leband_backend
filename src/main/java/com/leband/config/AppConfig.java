package com.leband.config;

import com.leband.oAuth.service.Impl.OAuthServiceImpl;
import com.leband.oAuth.service.OAuthService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    @Bean
    public JwtTokenProvider createJwtTokenProvider() {
        return new JwtTokenProvider();
    }

    @Bean
    public OAuthService oAuthService() {
        return new OAuthServiceImpl(createJwtTokenProvider());
    }
}
