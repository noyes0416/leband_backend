package com.leband.config;

import com.leband.common.JwtAuthenticationToken;
import com.leband.oAuth.service.Impl.OAuthServiceImpl;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JwtAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private final OAuthServiceImpl oAuthServiceImpl;

    public JwtAuthenticationFilter(OAuthServiceImpl oAuthServiceImpl) {
        super(new AntPathRequestMatcher("/v4/pool/**"));
        this.oAuthServiceImpl = oAuthServiceImpl;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) {
        String code = request.getParameter("code");

        String jwtToken = oAuthServiceImpl.generateJwtToken(code);

        if (jwtToken != null) {
            return new JwtAuthenticationToken(jwtToken);
        } else {
            return null;
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        SecurityContextHolder.getContext().setAuthentication(authResult);
        chain.doFilter(request, response);
    }
}
