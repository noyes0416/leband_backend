package com.leband.config;
import com.leband.oAuth.service.Impl.OAuthServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final OAuthServiceImpl oAuthServiceImpl;

    @Autowired
    public SecurityConfig(OAuthServiceImpl oAuthServiceImpl) {
        this.oAuthServiceImpl = oAuthServiceImpl;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                .antMatchers("/v2/api-docs", "/configuration/**", "/swagger*/**", "/webjars/**").permitAll()
                .antMatchers("/v4/member/naming").permitAll()
                .antMatchers("/v4/board/**").permitAll()
                .antMatchers("/v4/oauth/**").permitAll()
                .antMatchers("/main.html").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/main.html") // 로그인 페이지를 /main.html로 설정
                .permitAll()
                .and()
                .addFilterBefore(new JwtAuthenticationFilter(oAuthServiceImpl), UsernamePasswordAuthenticationFilter.class);
    }
}
