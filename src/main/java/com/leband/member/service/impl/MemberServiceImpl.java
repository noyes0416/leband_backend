package com.leband.member.service.impl;
import com.leband.member.dto.MemberDto;
import com.leband.board.entity.NamingEntity;
import com.leband.board.repository.NamingRepository;
import com.leband.member.entity.MemberEntity;
import com.leband.member.repository.MemberRepository;
import com.leband.member.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

@Service
public class MemberServiceImpl implements MemberService {

    private final MemberRepository memberRepository;

    @Autowired
    public MemberServiceImpl(MemberRepository memberRepository, NamingRepository namingRepository) {
        this.memberRepository = memberRepository;
    }

    @Override
    public Long getMemberInfo(MemberDto memberDto) {
        MemberEntity existingMember = memberRepository.findById(memberDto.getId()).orElse(null);

        if (existingMember != null) {
            return existingMember.getId();
        } else {
            MemberEntity newMember = new MemberEntity();
            newMember.setId(memberDto.getId());
            newMember.setRegDt(LocalDateTime.now());

            MemberEntity savedMember = memberRepository.save(newMember);
            return savedMember.getId();
        }
    }
}
