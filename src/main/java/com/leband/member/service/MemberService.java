package com.leband.member.service;

import com.leband.member.dto.MemberDto;

public interface MemberService {
    Long getMemberInfo(MemberDto memberDto);
}
