package com.leband.member.dto;

import lombok.*;

import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class MemberDto {
    private Long id;
    private String nickname;
    private String profileImageUrl;
    private String thumbnailImageUrl;
}
