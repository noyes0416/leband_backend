package com.leband.member.controller;

import com.leband.common.constants.URLConstants;
import com.leband.member.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = URLConstants.Member.DEFAULT_MEMBER_URL)
public class MemberController {

    private final MemberService memberService;

}
