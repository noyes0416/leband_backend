package com.leband.pool.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Table(name = "TPool")
@Entity
public class PoolEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String location;

    private LocalDateTime createdAt;

    // Getter, Setter, 생성자 등 생략
}
