package com.leband.oAuth.service.Impl;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.leband.config.JwtTokenProvider;
import com.leband.member.dto.MemberDto;
import com.leband.oAuth.service.OAuthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

@Service
public class OAuthServiceImpl implements OAuthService {
    private static final Logger logger = LoggerFactory.getLogger(OAuthServiceImpl.class);
    private static final String REST_API_KEY = "bcdd250dfdd9c4e0e1b502d4b8cf5b4c";
    private static final String REDIRECT_URI = "http://localhost:8080/v4/oauth/callback";
    private final JwtTokenProvider jwtTokenProvider;

    @Autowired
    public OAuthServiceImpl(JwtTokenProvider jwtTokenProvider) {
        this.jwtTokenProvider = jwtTokenProvider;
    }

//TODO : SDK 연동 시 불필요 하므로 추후 프론트 구성 후 필요에 따라 삭제 예정

//    public String requestOAuthToken(String authorizeCode) {
//        String accessToken = "";
//        String reqURL = "https://kauth.kakao.com/oauth/token";
//
//        try {
//            URL url = new URL(reqURL);
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//
//            conn.setRequestMethod("POST");
//            conn.setDoOutput(true);
//
//            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream()));
//            StringBuilder sb = new StringBuilder();
//            sb.append("grant_type=authorization_code");
//            sb.append("&client_id=" + REST_API_KEY);
//            sb.append("&redirect_uri=" + REDIRECT_URI);
//            sb.append("&code=" + authorizeCode);
//            bw.write(sb.toString());
//            bw.flush();
//
//            String result = getRequestResult(conn);
//
//            JsonParser parser = new JsonParser();
//            JsonElement element = parser.parse(result);
//
//            accessToken = element.getAsJsonObject().get("access_token").getAsString();
//
//            bw.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return accessToken;
//    }

    public MemberDto getKakaoUserInfo(String token) {
        String reqURL = "https://kapi.kakao.com/v2/user/me";
        MemberDto member = new MemberDto();

        try {
            URL url = new URL(reqURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod("GET");
            conn.setDoOutput(true);
            conn.setRequestProperty("Authorization", "Bearer " + token);

            JsonParser parser = new JsonParser();
            try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                String result = br.readLine();
                JsonElement element = parser.parse(result);
                JsonElement kakaoAccount = element.getAsJsonObject().get("kakao_account");
                JsonElement profile = kakaoAccount.getAsJsonObject().get("profile");

                member.setId(element.getAsJsonObject().get("id").getAsLong());
                member.setNickname(profile.getAsJsonObject().get("nickname").getAsString());
                member.setProfileImageUrl(profile.getAsJsonObject().get("profile_image_url").getAsString());
                member.setThumbnailImageUrl(profile.getAsJsonObject().get("thumbnail_image_url").getAsString());

            }
        } catch (IOException e) {
            logger.debug(e.getMessage());
        }

        return member;
    }

    public String generateJwtToken(String accessToken) {
        String jwtToken = jwtTokenProvider.generateToken("kakao_" + accessToken);
        return jwtToken;
    }

    private String getRequestResult(HttpURLConnection conn) throws IOException {
        StringBuilder sb = new StringBuilder();
        if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
            }
        } else {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getErrorStream(), StandardCharsets.UTF_8))) {
                String line;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
            }
        }
        return sb.toString();
    }
}