package com.leband.oAuth.service;

import com.leband.member.dto.MemberDto;

public interface OAuthService {
//    String requestOAuthToken(String authorizeCode);
    MemberDto getKakaoUserInfo(String token);
    String generateJwtToken(String authorizeCode);
}
