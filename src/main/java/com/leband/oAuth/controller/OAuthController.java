package com.leband.oAuth.controller;

import com.leband.common.constants.URLConstants;
import com.leband.member.dto.MemberDto;
import com.leband.member.service.MemberService;
import com.leband.oAuth.service.Impl.OAuthServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = URLConstants.OAuth.DEFAULT_OAUTH_URL)
public class OAuthController {

    private final OAuthServiceImpl oAuthServiceImpl;

    @Autowired
    public OAuthController(OAuthServiceImpl oAuthServiceImpl, MemberService memberService) {
        this.oAuthServiceImpl = oAuthServiceImpl;
    }

    @PostMapping(value = URLConstants.OAuth.CALLBACK)
    public ResponseEntity<Map<String, Object>> oAuthCallBack(@RequestParam("code") String accessToken) {
            if (accessToken != null) {
                MemberDto member = oAuthServiceImpl.getKakaoUserInfo(accessToken);
                String jwtToken = oAuthServiceImpl.generateJwtToken(accessToken);

                Map<String, Object> responseData = new HashMap<>();
                responseData.put("jwtToken", jwtToken);
                responseData.put("member", member);

                return ResponseEntity.status(HttpStatus.OK).header(HttpHeaders.LOCATION, "/main.html").body(responseData);
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
            }
    }
}
