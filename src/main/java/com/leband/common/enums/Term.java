package com.leband.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/*
    합주 주기 (일, 주, 월)
 */

@Getter
@RequiredArgsConstructor
public enum Term {

    DAY(1),
    WEEK(2),
    MONTH(3);

    private final Integer value;
}
