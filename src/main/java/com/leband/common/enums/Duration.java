package com.leband.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/*
    기간 (단기/장기)
 */

@Getter
@RequiredArgsConstructor
public enum Duration {

    SHORT(1),
    LONG(2);

    private final Integer value;
}
