package com.leband.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/*
    악기 종류
 */
@Getter
@RequiredArgsConstructor
public enum  Instrument {

    PIANO(1),
    DRUM(2),
    GUITAR(3),
    BASS(4),
    VOCAL(5);

    private final Integer value;
}
