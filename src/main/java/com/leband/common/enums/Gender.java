package com.leband.common.enums;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

/*
    성별
 */

@Getter
@RequiredArgsConstructor
public enum Gender {

    MAN(1),
    WOMAN(2);

    private final Integer value;
}
