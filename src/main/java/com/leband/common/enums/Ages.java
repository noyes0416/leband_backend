package com.leband.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/*
    연령대
 */

@Getter
@RequiredArgsConstructor
public enum Ages {

    AGES10(1),
    AGES20(2),
    AGES30(3),
    AGES40(4),
    AGES50(5),
    OVER50(6);

    private final Integer value;
}
