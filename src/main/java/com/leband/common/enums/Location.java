package com.leband.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/*
    지역
 */

@Getter
@RequiredArgsConstructor
public enum Location {

    SEOUL(1),
    GYEONGGI(2),
    GANGWON(3),
    CHUNGCHUNGNORTH(4),
    CHUNGCHUNGSOUTH(5),
    JEOLLANORTH(6),
    JEOLLASOUTH(7),
    GYEONGSANGNORTH(8),
    GYEONGSANGSOUTH(9),
    JEJU(10);


    private final Integer value;
}
