package com.leband.common.enums;

import com.leband.common.exception.LeBandException;
import lombok.Getter;

import java.util.Arrays;
import java.util.stream.Stream;

@Getter
public enum UseState {
    NORMAL(1),
    ABNORMAL(2),
    CLOSED(3);

    private final Integer value;

    UseState(Integer value) {
        this.value = value;
    }

    public int getIntValue() {
        return value;
    }

    public static UseState isValidUseStateRange(Integer useState) {
        return Arrays.stream(UseState.values())
                .filter(val -> val.getIntValue() == useState)
                .findAny()
                .orElseThrow(() -> new LeBandException(ErrorCode.NOT_VALID_USESATE_RANGE));
    }
}
