package com.leband.common.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

/*
    * 1000 ~ 2000번대 : Board
    * 9000 ~ 9999번대 : Common
 */

@Getter
@RequiredArgsConstructor
public enum ErrorCode {

    /* == BOARD == */
    NOT_EXISTS_VALID_BOARD(HttpStatus.OK, 1000, "Valid Board is not exists."),

    /* == Common == */
    NOT_VALID_USESATE_RANGE(HttpStatus.OK, 9000, "UseState Range IS INVALID");

    private final HttpStatus status;
    private final Integer code;
    private final String message;
}
