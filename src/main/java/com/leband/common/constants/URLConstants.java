package com.leband.common.constants;

public class URLConstants {

    public static final String DEFAULT_URL = "/v4";

    public class Board {

        public static final String DEFAULT_BOARD_URL = DEFAULT_URL + "/board";
        public static final String CREATE = "/create";
        public static final String UPDATE = "/update";
        public static final String DELETE = "/delete";
        public static final String UPDATE_STATE = "/update/state";
        public static final String RENEW = "/renew";
        public static final String LIST = "/list";
        public static final String NAMING = "/naming";
        public static final String MY_BOARD_LIST = "/list/my";
    }

    public class OAuth {
        public static final String DEFAULT_OAUTH_URL = DEFAULT_URL + "/oauth";
        public static final String CALLBACK = "/callback";
    }

    public class Member {
        public static final String DEFAULT_MEMBER_URL = DEFAULT_URL + "/member";
        public static final String NAMING = "/naming";
    }
}
