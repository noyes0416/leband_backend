package com.leband.common.aspect;

import com.leband.common.exception.ErrorResponse;
import com.leband.common.exception.LeBandException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class LeBandExceptionHandler {

    @ExceptionHandler(LeBandException.class)
    public ResponseEntity<ErrorResponse> handleCustomException(LeBandException e) {

        return ErrorResponse.error(e);
    }
}
