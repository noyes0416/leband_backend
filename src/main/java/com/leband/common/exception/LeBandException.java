package com.leband.common.exception;

import com.leband.common.enums.ErrorCode;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class LeBandException extends RuntimeException {

    private final ErrorCode errorCode;

    public LeBandException(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }
}
