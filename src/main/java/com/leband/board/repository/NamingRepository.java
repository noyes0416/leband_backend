package com.leband.board.repository;

import com.leband.board.entity.NamingEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NamingRepository extends JpaRepository<NamingEntity, Long> {
    List<NamingEntity> findBySort(int i);
}
