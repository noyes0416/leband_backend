package com.leband.board.repository;

import com.leband.board.entity.TBoardEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BoardRepository extends JpaRepository<TBoardEntity, Long>, JpaSpecificationExecutor<TBoardEntity> {

    Optional<TBoardEntity> findByBoardNoAndUserNoAndUseState(Long boardNo, Long userNo, Integer useState);

    List<TBoardEntity> findAllByUserNoOrderByRegDate(Long userNo);
}
