package com.leband.board.repository;

import com.leband.board.entity.TBoardEntity;
import com.leband.common.enums.UseState;
import org.springframework.data.jpa.domain.Specification;

public class BoardSpecification {

    public static Specification<TBoardEntity> likeBoardTitle(String boardTitle){
        return (root, query, criteriaBuilder) -> criteriaBuilder.like(root.get("boardTitle"), "%" + boardTitle + "%");
    }

    public static Specification<TBoardEntity> equalUseState(UseState useState){
        return (root, query, criteriaBuilder) -> criteriaBuilder.equal(root.get("useState"), useState.getIntValue());
    }
}
