package com.leband.board.controller;

import com.leband.board.dto.request.*;
import com.leband.board.dto.response.BoardListResponseDTO;
import com.leband.board.dto.response.MyBoardListResponseDTO;
import com.leband.board.service.BoardService;
import com.leband.common.constants.URLConstants;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = URLConstants.Board.DEFAULT_BOARD_URL)
public class BoardController {

    private final BoardService boardService;

    @PostMapping(value = URLConstants.Board.CREATE)
    public void createBoard(@Valid @RequestBody CreateBoardRequestDTO requestDTO) {

        boardService.createBoard(requestDTO);
    }

    @PostMapping(value = URLConstants.Board.UPDATE)
    public void updateBoard(@Valid @RequestBody UpdateBoardRequestDTO requestDTO) {

        boardService.updateBoard(requestDTO);
    }

    @PostMapping(value = URLConstants.Board.DELETE)
    public void deleteBoard(@Valid @RequestBody DeleteBoardRequestDTO requestDTO) {

        boardService.deleteBoard(requestDTO);
    }

    @PostMapping(value = URLConstants.Board.UPDATE_STATE)
    public void updateBoardState(@Valid @RequestBody UpdateBoardStateRequestDTO requestDTO) {

        boardService.updateBoardState(requestDTO);
    }

    @PostMapping(value = URLConstants.Board.RENEW)
    public void renewBoard(@Valid @RequestBody RenewBoardRequestDTO requestDTO) {

        boardService.renewBoard(requestDTO);
    }

    @GetMapping(value = URLConstants.Board.LIST)
    public List<BoardListResponseDTO> boardList(@RequestParam(required = false) String boardTitle) {

        return boardService.getActiveBoardList(boardTitle).stream()
                .map(it -> BoardListResponseDTO.of(it))
                .collect(Collectors.toList());
    }

    @GetMapping(value = URLConstants.Board.NAMING)
    public String getBandName() {

        return boardService.getBandName();
    }

    @GetMapping(value = URLConstants.Board.MY_BOARD_LIST)
    public List<MyBoardListResponseDTO> myBoardList(@RequestParam(required = false) Long userNo) {

        return boardService.getMyBoardList(userNo).stream()
                .map(it -> MyBoardListResponseDTO.of(it))
                .collect(Collectors.toList());
    }
}
