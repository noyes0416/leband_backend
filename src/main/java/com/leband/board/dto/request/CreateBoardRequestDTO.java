package com.leband.board.dto.request;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Builder
@Getter
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class CreateBoardRequestDTO {

    @NotNull
    Long userNo;

    @NotEmpty
    String contactUrl;

    @NotEmpty
    String boardTitle;

    Integer positionNo;

    Integer locationNo;
}
