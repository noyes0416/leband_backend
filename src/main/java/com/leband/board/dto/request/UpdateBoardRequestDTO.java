package com.leband.board.dto.request;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Builder
@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class UpdateBoardRequestDTO {

    @NotNull
    Long boardNo;

    @NotNull
    Long userNo;

    @NotEmpty
    String contactUrl;

    @NotEmpty
    String boardTitle;

    Integer positionNo;

    Integer locationNo;
}
