package com.leband.board.dto.request;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;

@Getter
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class UpdateBoardStateRequestDTO {

    @NotNull
    Long boardNo;

    @NotNull
    Long userNo;

    @NotNull
    Integer useState;
}
