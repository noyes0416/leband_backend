package com.leband.board.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.leband.board.entity.TBoardEntity;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class BoardListResponseDTO {

    Long boardNo;
    Long version;
    Long userNo;
    String contactUrl;
    String boardTitle;
    Integer positionNo;
    Integer locationNo;
    Integer useState;
    LocalDateTime regDate;
    LocalDateTime updDate;

    public static BoardListResponseDTO of(TBoardEntity entity) {
        return BoardListResponseDTO.builder()
                .boardNo(entity.getBoardNo())
                .version(entity.getVersion())
                .userNo(entity.getUserNo())
                .contactUrl(entity.getContactUrl())
                .boardTitle(entity.getBoardTitle())
                .positionNo(entity.getPositionNo())
                .locationNo(entity.getLocationNo())
                .useState(entity.getUseState())
                .regDate(entity.getRegDate())
                .updDate(entity.getUpdDate())
                .build();
    }
}
