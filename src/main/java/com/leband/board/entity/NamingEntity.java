package com.leband.board.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Table(name = "TNaming")
@Entity
public class NamingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idx;

    @Column(name = "sort")
    private int sort;

    @Column(name = "name", nullable = true, length = 50)
    private String name;

    public void setIdx(Long idx) {
        this.idx = idx;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NamingEntity that = (NamingEntity) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
