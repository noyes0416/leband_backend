package com.leband.board.entity;

import com.leband.board.dto.request.*;
import com.leband.common.entity.BaseEntity;
import com.leband.common.enums.UseState;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Table(name = "TBoard")
@Entity
public class TBoardEntity extends BaseEntity {

    @Id
    @Column(name = "board_no")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long boardNo;

    @NotNull
    @Column(name = "board_version")
    private Long version;

    @NotNull
    @Column(name = "user_no")
    private Long userNo;

    @NotNull
    @Column(name = "contact_url", length = 100)
    private String contactUrl;

    @NotNull
    @Column(name = "board_title", length = 20)
    private String boardTitle;

    @Column(name = "position_no")
    private Integer positionNo;

    @Column(name = "location_no")
    private Integer locationNo;

    @NotNull
    @Column(name = "use_state")
    private Integer useState;

    public static TBoardEntity createInsertEntity(CreateBoardRequestDTO requestDTO) {
        return TBoardEntity.builder()
                .version(1L)
                .userNo(requestDTO.getUserNo())
                .contactUrl(requestDTO.getContactUrl())
                .boardTitle(requestDTO.getBoardTitle())
                .positionNo(requestDTO.getPositionNo())
                .locationNo(requestDTO.getLocationNo())
                .useState(UseState.NORMAL.getIntValue())
                .build();
    }

    public void toUpdateEntity(UpdateBoardRequestDTO requestDTO) {
        ++this.version;
        this.contactUrl = requestDTO.getContactUrl();
        this.boardTitle = requestDTO.getBoardTitle();
        this.positionNo = requestDTO.getPositionNo();
        this.locationNo = requestDTO.getLocationNo();
    }

    public void toDeleteEntity() {
        ++this.version;
        this.useState = UseState.ABNORMAL.getIntValue();
    }

    public void toUpdateStateEntity(UpdateBoardStateRequestDTO requestDTO) {
        ++this.version;
        this.useState = requestDTO.getUseState();
    }

    public void toRenewEntity() {
        ++this.version;
    }
}
