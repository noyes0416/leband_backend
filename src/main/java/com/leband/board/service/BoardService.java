package com.leband.board.service;

import com.leband.board.dto.request.*;
import com.leband.board.entity.NamingEntity;
import com.leband.board.entity.TBoardEntity;
import com.leband.board.repository.BoardRepository;
import com.leband.board.repository.BoardSpecification;
import com.leband.board.repository.NamingRepository;
import com.leband.common.enums.ErrorCode;
import com.leband.common.enums.UseState;
import com.leband.common.exception.LeBandException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class BoardService {

    private final BoardRepository boardRepository;
    private final NamingRepository namingRepository;

    @Transactional
    public void createBoard(CreateBoardRequestDTO requestDTO) {

        boardRepository.save(TBoardEntity.createInsertEntity(requestDTO));
    }

    @Transactional
    public void updateBoard(UpdateBoardRequestDTO requestDTO) {

        getExistsNormalBoard(requestDTO.getBoardNo(), requestDTO.getUserNo())
                .toUpdateEntity(requestDTO);
    }

    @Transactional
    public void deleteBoard(DeleteBoardRequestDTO requestDTO) {

        getExistsNormalBoard(requestDTO.getBoardNo(), requestDTO.getUserNo())
                .toDeleteEntity();
    }

    @Transactional
    public void updateBoardState(UpdateBoardStateRequestDTO requestDTO) {

        UseState.isValidUseStateRange(requestDTO.getUseState());

        getExistsNormalBoard(requestDTO.getBoardNo(), requestDTO.getUserNo())
                .toUpdateStateEntity(requestDTO);
    }

    @Transactional
    public void renewBoard(RenewBoardRequestDTO requestDTO) {

        getExistsNormalBoard(requestDTO.getBoardNo(), requestDTO.getUserNo())
                .toRenewEntity();
    }

    private TBoardEntity getExistsNormalBoard(Long boardNo, Long userNo) {
        return boardRepository.findByBoardNoAndUserNoAndUseState(boardNo, userNo, UseState.NORMAL.getIntValue())
                .orElseThrow(() -> new LeBandException(ErrorCode.NOT_EXISTS_VALID_BOARD));
    }

    public List<TBoardEntity> getActiveBoardList(String boardTitle) {

        Specification<TBoardEntity> spec = (root, query, criteriaBuilder) -> null;

        //where
        spec = (!Objects.isNull(boardTitle)) ? spec.and(BoardSpecification.likeBoardTitle(boardTitle)) : spec;
        spec = spec.and(BoardSpecification.equalUseState(UseState.NORMAL));

        return boardRepository.findAll(spec).stream()
                .sorted(Comparator.comparing(TBoardEntity::getUpdDate)
                                .thenComparing(TBoardEntity::getRegDate).reversed())
                .collect(Collectors.toList());
    }

    public String getBandName() {
        List<NamingEntity> order1Names = namingRepository.findBySort(1);
        List<NamingEntity> order2Names = namingRepository.findBySort(2);

        Random random = new Random();
        NamingEntity name1 = order1Names.get(random.nextInt(order1Names.size()));
        NamingEntity name2 = order2Names.get(random.nextInt(order2Names.size()));

        return name1.getName() + " " + name2.getName();
    }

    public List<TBoardEntity> getMyBoardList(Long userNo) {

        return boardRepository.findAllByUserNoOrderByRegDate(userNo);
    }
}