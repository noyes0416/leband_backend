CREATE TABLE IF NOT EXISTS TBoard
(
    board_no      BIGINT AUTO_INCREMENT PRIMARY KEY,
    board_version BIGINT NOT NULL,
    user_no       BIGINT NOT NULL,
    contact_url   VARCHAR(100) NOT NULL,
    board_title   VARCHAR(20) NOT NULL,
    position_no   INT,
    location_no   INT,
    use_state     INT NOT NULL,
    reg_date      DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
    upd_date      DATETIME
);
