CREATE TABLE IF NOT EXISTS TPool
(
    id          BIGINT AUTO_INCREMENT PRIMARY KEY,
    name        VARCHAR(255) NOT NULL,
    location    VARCHAR(255),
    created_at  DATETIME
);
