CREATE TABLE IF NOT EXISTS TMember
(
    idx         bigint auto_increment comment 'index' primary key,
    id          mediumtext                           not null comment 'kakao id',
    token       varchar(100)                         not null comment 'kakao auth token',
    status      tinyint  default 0                   not null comment 'member status(0:normal, 1:suspend, 2:withdrawal)',
    regDt       datetime default current_timestamp() not null comment 'register datetime',
    uptDt       datetime                             null comment 'update datetime',
    lastLoginDt datetime                             null comment 'last login datetime'
)
collate = latin1_swedish_ci;