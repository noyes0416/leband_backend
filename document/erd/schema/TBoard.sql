CREATE TABLE IF NOT EXISTS TBoard (
    board_no BIGINT AUTO_INCREMENT PRIMARY KEY,
    board_version BIGINT DEFAULT(1) NOT NULL,
    user_no  BIGINT NOT NULL,
    contact_url VARCHAR(100) NOT NULL,
    board_title VARCHAR(20) NOT NULL,
    position_no BIGINT,
    location_no BIGINT,
    use_state SMALLINT(3) NOT NULL,
    reg_date DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
    upd_date DATETIME
);